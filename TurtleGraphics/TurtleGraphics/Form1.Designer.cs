﻿namespace TurtleGraphics
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.draw = new System.Windows.Forms.Button();
            this.SAVE = new System.Windows.Forms.Button();
            this.setBkgrd = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.loadArea = new System.Windows.Forms.Button();
            this.cmdBox = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.colorDialog2 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // draw
            // 
            this.draw.Location = new System.Drawing.Point(12, 12);
            this.draw.Name = "draw";
            this.draw.Size = new System.Drawing.Size(75, 23);
            this.draw.TabIndex = 0;
            this.draw.Text = "Draw";
            this.draw.UseVisualStyleBackColor = true;
            this.draw.Click += new System.EventHandler(this.draw_Click);
            // 
            // SAVE
            // 
            this.SAVE.Location = new System.Drawing.Point(12, 41);
            this.SAVE.Name = "SAVE";
            this.SAVE.Size = new System.Drawing.Size(75, 23);
            this.SAVE.TabIndex = 1;
            this.SAVE.Text = "Save";
            this.SAVE.UseVisualStyleBackColor = true;
            this.SAVE.Click += new System.EventHandler(this.SAVE_Click);
            // 
            // setBkgrd
            // 
            this.setBkgrd.Location = new System.Drawing.Point(12, 70);
            this.setBkgrd.Name = "setBkgrd";
            this.setBkgrd.Size = new System.Drawing.Size(75, 23);
            this.setBkgrd.TabIndex = 2;
            this.setBkgrd.Text = "BackGround";
            this.setBkgrd.UseVisualStyleBackColor = true;
            this.setBkgrd.Click += new System.EventHandler(this.setBkgrd_Click);
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(12, 99);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 3;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // loadArea
            // 
            this.loadArea.Location = new System.Drawing.Point(12, 128);
            this.loadArea.Name = "loadArea";
            this.loadArea.Size = new System.Drawing.Size(75, 23);
            this.loadArea.TabIndex = 4;
            this.loadArea.Text = "Load";
            this.loadArea.UseVisualStyleBackColor = true;
            this.loadArea.Click += new System.EventHandler(this.loadArea_Click);
            // 
            // cmdBox
            // 
            this.cmdBox.Location = new System.Drawing.Point(12, 157);
            this.cmdBox.Name = "cmdBox";
            this.cmdBox.Size = new System.Drawing.Size(100, 20);
            this.cmdBox.TabIndex = 5;
            this.cmdBox.TextChanged += new System.EventHandler(this.cmdBox_TextChanged);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "penDown",
            "penUp",
            "forward",
            "backward",
            "right",
            "left",
            "reset",
            "clear",
            "triangle",
            "moveto"});
            this.listBox1.Location = new System.Drawing.Point(14, 184);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(99, 160);
            this.listBox1.TabIndex = 6;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(118, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(670, 426);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 543);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.cmdBox);
            this.Controls.Add(this.loadArea);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.setBkgrd);
            this.Controls.Add(this.SAVE);
            this.Controls.Add(this.draw);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button draw;
        private System.Windows.Forms.Button SAVE;
        private System.Windows.Forms.Button setBkgrd;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button loadArea;
        private System.Windows.Forms.TextBox cmdBox;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ColorDialog colorDialog2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

