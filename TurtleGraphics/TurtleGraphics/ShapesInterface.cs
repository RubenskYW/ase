﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace TurtleGraphics
{
    interface Shapes    
    {
        void set(Color c, params int[] list);

        void draw(Graphics graphics);

        double calcArea();

        double calPerimeter();
    }
}
