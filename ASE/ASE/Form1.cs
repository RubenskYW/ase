﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace ASE
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //if the check box is checked 
            if (checkBox1.Checked)
                // Stretch image 
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            //else if unchecked leave it normal
            else
                pictureBox1.SizeMode = PictureBoxSizeMode.Normal;
            
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        //showButton
        private void button1_Click(object sender, EventArgs e) // Event handler for Show Picture Button
        {
            /*
              * Using ShowDialog() Method 
              * Runs a common dialog box with a default owner.
              */
            if (openFileDialog1.ShowDialog() == DialogResult.OK)//if returns OK if user clicks okay
            {
                pictureBox1.Load(openFileDialog1.FileName); //load the file chosen
            }

        }

        //clearButton
        private void button2_Click(object sender, EventArgs e) // Event handler for Clear Button
        {

            pictureBox1.Image = null; //null clears the image from the pictureBox1
        }

        //backgroundButton
        private void button3_Click(object sender, EventArgs e) // Event handler for Set Background Button
        {

            if (colorDialog1.ShowDialog() == DialogResult.OK) // if statment with ShowDialog() method
                // pick a color using "colorDialog"
                pictureBox1.BackColor = colorDialog1.Color;
        }

        //closeButton
        private void button4_Click(object sender, EventArgs e) // Event handler for Close Button
        {
            this.Close();// current instance and carries out the Close() method
        }

        //Created a open a file object 
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            
        }
    }
}
